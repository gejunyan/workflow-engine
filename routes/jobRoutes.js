const { promisify } = require('util');
const bodyParser = require("body-parser");
const URL = require('url');
const keys = require('../config/keys');
const NODE_ENV = process.env.NODE_ENV;
const Bull = require("bull");
const QUEUE_NAME= 'FLOW';
const TASK_QUEUE = 'TASK@' + NODE_ENV;
const EMAIL_QUEUE = 'EMAIL@' + NODE_ENV;
const flowQueue = new Bull(QUEUE_NAME, keys.redisURL);
const taskQueue = new Bull(TASK_QUEUE, keys.redisURL);
const emailQueue = new Bull(EMAIL_QUEUE, keys.redisURL);
const Auth = require("../services/authentication");
const sample_flow_definition = require('../config/wf-definition-example.json');
const MessagingResponse = require('twilio').twiml.MessagingResponse;
const redis = require('redis');
const async = require('async');
const redisqueries = require('../services/redisqueries');
const taskqueries = require('../services/taskqueries');
const { doesNotMatch } = require('assert');


module.exports = app => {
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());

  app.get('/allkeys/:id', async function(req, res) {
	console.log(req.params.id)
	redisqueries.allkeys(`bull:FLOW:${req.params.id}*`)
		.then(keys => {
			res.json({"status": true, "message": keys, "status_code": 200})
		})
		.catch(alert => {
			res.json({ "status": false, "message": alert.message, "status_code": 401})
		})
  })

  app.get('/allIds', async function(req, res) {
	redisqueries.allIds(resData => {
		res.send(resData)
	})
	})

  app.get('/queues', async function(req, res) {
	redisqueries.scan(resData => {
		res.send(resData)
	})
  })

  app.post('/orchestration', Auth.Authenticate, async function(req, res) {
	  console.log(req.headers)
	const url = URL.parse(req.url, true)
	const mode = url.query.mode;
	const jobDefinition = (mode && mode === "test")?sample_flow_definition: req.body; 
	redisqueries.instanceNumber('bull:FLOW:id')
		.then(uniqueId => {
			console.log(uniqueId);
			const JobOpts = {
				... jobDefinition._id && {jobId: jobDefinition._id + "-" + uniqueId}
			};
			console.log("Posting ", (mode && mode === "test")? "sample flow definition": "flow definition", JobOpts);
			jobDefinition.name = jobDefinition.workflowName;
			jobDefinition.tenant = req.headers.tenant;
			jobDefinition.state = "Queued";
			flowQueue.add(jobDefinition, JobOpts)
				.then(result => {
					console.log("jobId:", result.id, "jobState:", result.getState())
					res.json({"status": true, "data": result, "status_code": 200})
					}, error => {
					console.log("error:", error)
					res.json({ "status": false, "message": error.message, "status_code": 401 });
					})
				.catch(alert => {
					console.log("alert:", alert)
					res.json({ "status": false, "message": alert.message, "status_code": 401 });
				})
		})
		.catch(alert => {
			res.json({ "status": false, "message": alert.message, "status_code": 401})
		})
  })

  app.get('/orchestration/:id', Auth.Authenticate, function(req, res) {
	console.log(req.params.id)
	flowQueue.getJob(req.params.id)
		.then(job => {
			console.log("result:", job)
			job.getState()
				.then(result => {
					console.log("jobState:", result)
				})
				.catch(alert => {
					console.log("alert:", alert)
				})
				res.send(job)
			}, error => {
				console.log("(ops!)error:", error)
				res.send(error)
			})
		.catch(alert => {
			console.log("alert:", alert)
			res.send(alert)
		})
  })

  app.get('/logs/:jobId', Auth.Authenticate, function(req, res) {
	const jobId = req.params.jobId;
	const url = URL.parse(req.url, true);
	const start = url.query.start? url.query.start : 0;
	const end = url.query.end? url.query.end : 20;
	  flowQueue.getJobLogs(jobId, start, end)
	  	.then(logs => {
			  console.log(`jobLogs(${jobId}?${start}&${end}):`, logs)
			  res.json(logs);
		  }, error => {
			console.log("(ops!)error:", error)
			res.send(error)
		  })
		.catch(alert => {
			console.log("(ops!)alert:", alert);
			res.send(alert);
		})
  })

  app.post('/resumejob/:jobId/:outcome', Auth.Authenticate, async function(req, res) {
	const jobId = req.params.jobId;
	const job = await flowQueue.getJob(jobId);
	if (job.data.state !== "Paused") {
		res.send("Only a paused job could be resumed");
		return;
	}
	const jobData = {...job.data};
	jobData.definition.actions[0].configuration.properties.outcome = req.params.outcome;
	flowQueue.getJobLogs(jobId)
		.then(logs => {
			const jobLogs = {...logs}
			console.log("jobLogs123:", jobLogs);
			job.remove();
			flowQueue.add(jobData, {jobId: jobId})
				.then(resumedJob => {
					jobLogs.logs.forEach(log => {
						resumedJob.log(log);
					});
				})
				.then(resumedJob => {
					res.send(resumedJob)
				})
		})

  })

  app.get('/instances/:flowId', Auth.Authenticate, function(req, res) {
	const flowId = req.params.flowId;

	redisqueries.allkeys(`bull:FLOW:${flowId}-*[^s]`)
		.then(async keys => {
				//console.log(keys);
				const instList = []
				var inst = {}
				var getJobList = new Promise((resolve, reject) => {
					strRegex = new RegExp(`bull\\:${QUEUE_NAME}\\:(.*)`);
					keys.forEach(async (key, i, array) => {
						//console.log(key, i)
						//if (!key.endsWith(":logs")) {
							//inst = await flowQueue.getJob(key.match(/bull\:FLOW\:(.*)/)[1])
							inst = await flowQueue.getJob(key.match(strRegex)[1])
							instList.push(inst)
						//}
						if (i === array.length -1) resolve();
					})
				})
				
				getJobList.then(() => {
					console.log(`Log instances for ${flowId}:`, instList.length);
					if (instList.length > 0) {
						res.json({"status": true, "data":instList, "status_code": 200})
					} else {
						res.json({"status": false, "data": [], "status_code": 401})
					}
				})
			}, error => {
				console.log("error:", error);
				res.json({ "status": false, "message": "Found no matching keys", "status_code": 401 });
			})
		.catch(alert => {
			console.log("(ops!)alert:", alert);
			res.json({ "status": false, "message": alert.message, "status_code": 401})
		})
	
  })

  app.get('/task/:id', function(req, res) {
	const id = req.params.id;
	console.log("Retriving task:", id);/*
	var task = await taskQueue.getJob(id); 
		.then(task => {
			console.log(`Found task id: ${id}`, task)
			res.status(200).send(task)
		}).catch(err => {
			console.log(`Error retrieving task...${err}`)
			res.status(501).send({status: 501, error: err})
		}) */
	taskQueue.getJob(id)
		.then(task => {
			console.log(`Found task id: ${id}`, task)
			res.status(200).send(task)
		}).catch(err => {
			console.log(`Error retrieving task...${err}`)
			res.status(501).send({status: 501, error: err})
		})
	//console.log(`task id: ${id}`, task)
	//res.status(200).send(task)
  })

  app.get('/tasks', function(req, res) {
	var owner = req.headers.owner ? req.headers.owner : "";
	console.log("owner", owner)
	var getKeys = new Promise(async (resolve, reject) => {
		var keys = [];
		var keylist = undefined
		try {
			keylist = await redisqueries.allkeys(`bull:${TASK_QUEUE}:${owner}-*`).catch(e => { reject(e) })
			keys = keys.concat(keylist) 
			console.log("key length:", keys.length)
			resolve(keys)
		} catch (err) {
			reject({ message: err.message, status: false })
		}
	});
	getKeys.then((allkeys) => {
		const taskList = [];
		var taskInst = undefined;
		var getTaskList = new Promise((resolve, reject) => {
			try {
				allkeys.forEach(async (key, i, array) => {
					console.log("Retriving task:", key, key.match(/([^:]+$)/)[0]); //substring after the last colon (i.e. :)
					taskInst = await taskQueue.getJob(key.match(/([^:]+$)/)[0]).catch(e => { reject(e) }); 
					//console.log(taskInst)
					taskInst && taskList.push({id: taskInst.id, timestamp: taskInst.timestamp, key: key, data: taskInst.data, task: taskInst});
					if (i === array.length -1) resolve(taskList);
				})
			} catch (err) {
				//reject({ message: err.message, status: false })
				reject(err)
			}
		})

		getTaskList.then((tl) => {
			res.status(200).send(tl)
		})

		getTaskList.catch(err => {
			console.log("getTaskList err", err)
			res.status(401).send({})
		})
	})
	.catch(alert => {
		console.log("(ops!) alert:", alert);
		res.json({ "status": false, "message": alert, "status_code": 401})
	})
  })

  app.patch('/task/:id/:outcome', Auth.Authenticate, async function(req, res) {
	const id = req.params.id;
	var outcome = req.params.outcome;
	var taskInst = undefined;
	console.log("Retriving task:", id, " outcome:", outcome);
	taskInst = await taskQueue.getJob(id);
	outcome = outcome.match(/App/i) ? 'approved': outcome.match(/Rej/i) ? 'rejected':outcome;
	console.log("User's response:", outcome)

	taskqueries.resume(taskInst, outcome)
		.then(async ans => {
			if (ans.resumed) {
				// completion criteria met, update other tasks...
				taskqueries.closePendingTasks(taskInst, outcome)
			}
			console.log("Resumed message:", ans)
			taskInst.data.status = "Completed";
			taskInst.data.response = outcome;
			taskInst.data.updated = Date.now();
			await taskInst.update(taskInst.data);
			res.status(200).send(`${ans}`);
		}).catch(err => {
			console.log(`Error patching task...${err}`)
			res.status(501).send({status: 501, error: err})
		})
  })

  app.patch('/externaltask/:id/:outcome', async function(req, res) {
	const id = req.params.id;
	var outcome = req.params.outcome;
	var taskInst = undefined;
	console.log("Retriving task:", id, " outcome:", outcome);
	taskInst = await taskQueue.getJob(id);
	outcome = outcome.match(/App/i) ? 'approved': outcome.match(/Rej/i) ? 'rejected':outcome;
	console.log("User's response:", outcome)

	taskqueries.resume(taskInst, outcome)
		.then(async ans => {
			if (ans.resumed) {
				// completion criteria met, update other tasks...
				taskqueries.closePendingTasks(taskInst, outcome)
			}
			console.log("Resumed message:", ans)
			taskInst.data.status = "Completed";
			taskInst.data.response = outcome;
			taskInst.data.updated = Date.now();
			await taskInst.update(taskInst.data);
			res.status(200).send(`${ans}`);
		}).catch(err => {
			console.log(`Error patching task...${err}`)
			res.status(501).send({status: 501, error: err})
		})
  })

  app.post('/email/notify', function(req, res) {
    let message = req.body;
    emailQueue.add(message)
        .then(result => {
            res.status(200).send("Success");
        })
        .catch(alert => {
            res.status(401).send(alert);
        })
  })

  app.post('/sms/reply', function (req, res) {
	const twiml = new MessagingResponse();
	//const smsCount = req.session.counter || 0;
	const msg = req.body.Body;
	//req.session.counter = smsCount + 1;
	console.log("BODY: ", req.body)
	var command = msg.match(/^task|tasks$/i) ? 'task' : msg.match(/^\?$/) ? '?' : msg;
	console.log('Command:', command);

	switch (command) {
	  case "?":
		  const replyMsg = "?: Command helps \
			  \ntask, tasks: Get list of pending tasks \
			  \napp[roved]: Approve a task \
			  \nrej[ected]: Reject a task"
		  console.log(`replyMsg: ${replyMsg}`)
		  twiml.message(replyMsg);
		  res.writeHead(200, {'Content-Type':'text/xml'});
		  res.end(twiml.toString());
		  break
	  default:
		  taskQueue.getJobs(['delayed'], 0, 100)
			  .then(async result => {
				  const outcome = command.match(/App/i) ? 'approved': command.match(/Rej/i) ? 'rejected':
					  command.match(/task/i) ? 'task': undefined;
				  console.log("User's response:", outcome);

				  var waitingJob = result.filter(obj => {return obj.data.to === req.body.From})
				  console.log(`Total: ${result.length}, # of waiting jobs for ${req.body.From}`, waitingJob.length)
				  var openJob = waitingJob.filter(obj => {return obj.data.status === 'New'})
				  if (outcome == "task") {
					  if (waitingJob.length<1) return 'There were no pending task for you'
					  return (openJob.length<1)? `There were no pending task for you` : openJob.map(x => `${x.id}, ${x.data.taskName}`).join('\n');
				  }
				  
				  if (outcome === undefined) return `Failed interprete your reply: ${msg}, reply "?" to get help`;
				  if (openJob.length<1) return `There were no pending task to ${outcome}`;

				  var replyMsg = "";
				  
				  return taskqueries.resume(openJob[0], outcome)
					  .then(async ans => {
						  console.log(`1. Resumed: ${ans.resumed}, message: ${ans.message}`);
						  if (ans.resumed) {
							  // completion criteria met, update other tasks...
							  taskqueries.closePendingTasks(waitingJob[0], outcome)
						  } 
						  
						  waitingJob[0].data.status = "Completed";
						  waitingJob[0].data.response = outcome;
						  waitingJob[0].data.updated = Date.now();
						  await waitingJob[0].update(waitingJob[0].data);
						  //await waitingJob[0].promote();
						  //await waitingJob[0].moveToCompleted('completed', true, true)
						  //await waitingJob[0].remove();
						  return `${ans.message}`;
							  
					  }).catch(err => {
						  console.log(`Error...${err} ${msg}`)
						  return `Error... ${err}`
					  })
			  })
			  .then(replyMsg =>{
				  console.log(`replyMsg: ${replyMsg}`)
				  twiml.message(replyMsg);
				  res.writeHead(200, {'Content-Type':'text/xml'});
				  res.end(twiml.toString());
			  })
			  .catch(alert => {
				  console.log("ops!alert:", alert);
				  twiml.message('Failed!');
				  res.writeHead(200, {'Content-Type':'text/xml'});
				  res.end(twiml.toString());
			  })
		  break
	}

  //console.log("SESSION: ", req.session)
  //res.set('Content-Type', 'text/xml')
})

}